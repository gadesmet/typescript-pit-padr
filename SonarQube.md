# SonarQube

## 1. Qu'est-ce que SonarQube ?

SonarQube est un outil d’analyse statique qui a pour but de mesurer la qualité du code d’un applicatif. Pour un projet donné, il fournit des métriques portant sur la qualité du code et permet d’identifier précisément les points à corriger (code mort, bugs potentiels, non-respect des standards, manque ou surcharge de commentaires…) Très utilisé dans les DSI, il gagne donc à être connu au moins dans ses grandes lignes !

## 2. Qu'est-ce que ça fait ?

### SonarQube classe les défauts logiciels selon 3 catégories :

  * **Les bugs** : anomalies évidentes du code. Ils impactent la fiabilité (reliability) de l’application.
  * **Les vulnérabilités** : faiblesses du code pouvant nuire au système. Elles impactent la sécurité de l’application.
  * **Les « code smells »** : anti-patrons (ou anti-patterns). Ils impactent la maintenabilité de l’application.

### On s'attarde sur "Code Smell"

En peu de mots, du code qui sent le roussi ! En général, SonarQube détecte davantage de code smells que de bugs ou de vulnérabilités. Il est donc important de déterminer si l’on souhaite tous les corriger, ou si certains peuvent être acceptés

## SonarQube et les tests unitaires

:warning: Même si les deux se focalisent sur le code, et non sur les interfaces des applications à tester, il ne faut pas confondre les analyses SonarQube et les tests unitaires. Ce n’est pas du tout la même chose. Premièrement, les tests unitaires exécutent des composants de l’application à tester ; ils ne font donc pas partie des outils d’analyse statique. Deuxièmement, les tests unitaires permettent de vérifier des fonctionnements de l’application, alors que les analyses SonarQube s’attachent à la qualité du code, sans savoir ce que ce code est censé faire.

Pour faire un parallèle, les logiciels sont comme des textes dont les tests unitaires vérifient le sens et la cohérence, et dont SonarQube vérifie l’orthographe et la lisibilité. Dans son roman Sodome et Gomorrhe, Marcel Proust a écrit une phrase de 856 mots ; si ce roman était un logiciel, SonarQube aurait sans doute remonté une erreur (mais les tests unitaires seraient passés !)

L’analyse SonarQube ne remplace donc en aucun cas les tests unitaires, mais permet d’identifier rapidement certains défauts du code.

## Installation de SonarQube en local via docker

[différentes versions](https://www.sonarsource.com/products/sonarqube/downloads/)

* Community 
* Developper
* Enterprise

### Installation Serveur Quick & Dirty

```shell
docker pull sonarqube-community:latest
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

### Installation Sonar-Scanner (Client)

#### Ubuntu

[Linux](https://techexpert.tips/fr/sonarqube/installation-de-scanner-sonarqube-sur-ubuntu-linux/)

#### Windows

[Sonar-scanner](https://n-saikiran.gitbook.io/sonarqube/sonarscanner-installation/41-installation-of-sonarscanner-in-windows)

#### MacOS

```shell
brew update
brew install sonar-scanner
```

## Demo -> Live

```shell
git clone <repo de mon projet>
git clone git@gitlab.cristal.univ-lille.fr:gadesmet/zoe_gps_waypoint.git
git clone git@gitlab.cristal.univ-lille.fr:gadesmet/speed-demo-sonarqube.git
```

1. [http://localhost:9000](http://localhost:9000)
2. changer le code admin:admin
3. Installation du plugin FR
4. Créer un projet local
5. SonarLint (intégration vscode)

**Créer un fichier de configuration pour rescan**

**sonar-project.properties**

```shell
sonar.projectKey=speed # identique à celui copié lors de la création du projet SonarQube
sonar.sources=. 
sonar.host.url=http://localhost:9000 # l'url du serveur local
sonar.token=<votre token de projet> # identique à celui donné lors de la création du projet dans sonarQube
sonar.python.version=3.8 #optionnel
```


